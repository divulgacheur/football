const express = require('express')
const app = express()
const port = 3005

const yaml = require('js-yaml');
const fs = require('fs');
var doc;

try {
    doc = yaml.load(fs.readFileSync('football.yaml', 'utf8'));
    striker_name = doc.data.striker.name;
    striker_picture_raw = doc.binaryData.striker.picture;
} catch (e) {
    console.log(e);
}

app.get('/', (req, res) => {
    res.send("<p>C'était le 26 mai 93" + "</p><p>" + striker_name + "</p>" + "<img src='data:image/png;base64, " + striker_picture_raw + "' />");
    console.log("C'était le 26 mai 93")
    console.log(striker_name);
})

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
})


// Get document, or throw exception on error


